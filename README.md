[![coverage report](https://gitlab.com/fahdikrie/badisveryown-ppw-story-repository-part-2/badges/master/coverage.svg)](https://gitlab.com/fahdikrie/badisveryown-ppw-story-repository-part-2/-/commits/master)
[![pipeline status](https://gitlab.com/fahdikrie/badisveryown-ppw-story-repository-part-2/badges/master/pipeline.svg)](https://gitlab.com/fahdikrie/badisveryown-ppw-story-repository-part-2/-/commits/master)

**PPW Story 7 – 10**
=====

Name : Fahdii Ajmalal Fikrie <br>
Student ID : 1906398370 <br>
PPW 2nd Term 2019/2020 <br>
[https://badisveryownfeed.herokuapp.com/](https://badisveryownfeed.herokuapp.com/)

<br>

**Story 7 — Django Functional Testing**
-----

Created a facebook rip-off (or twitter, if you say so) that lets user to update their status on the page. Implemented using the TDD approach, using both Unit & Functional Test. Challenge for this week is to add button to each status that able to change the status' bubble color everytime it's clicked

Link to App & Challenge : [Status Feed Page](https://badisveryownfeed.herokuapp.com/)

<br>

**Story 8 — Javascript & JQuery**
-----

Created an accordion using Javascript's library, JQuery. Challenge for this story is to add a switch-theme button. Skipped the challenge because i figure it'd be a hassle to think about how the new theme would be (mager styling trs ganti-ganti warnanya etc)

Link to App : [Accordion Page](https://badisveryownfeed.herokuapp.com/accordion/)

<br>

**Story 9 — Ajax & WebServices**
-----

Created an online book database using Google Books API. Implemented using Ajax & Django REST Framework

Link to App : [Book Catalogue Page](https://badisveryownfeed.herokuapp.com/bookcatalogue/)

<br>

**Story 10 — Django Authentication, Cookie, and Session**
-----

Created an Authentication System (login, sign up, & logout) using Django built-in auth system.

Link to Login Page : [Login Page](https://badisveryownfeed.herokuapp.com/auth/login/) <br>
Link to Sign up Page : [Sign up Page](https://badisveryownfeed.herokuapp.com/auth/signup/) <br>
