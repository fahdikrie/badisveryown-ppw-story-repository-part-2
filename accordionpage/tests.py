from django.test import TestCase, override_settings, Client, LiveServerTestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .apps import AccordionpageConfig
from .views import accordion, accordion_jqueryui


# Create your tests here.
class StatusFeedUnitTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_apps(self):
        self.assertEqual(AccordionpageConfig.name, 'accordionpage')

    def test_accordion_page_url_is_exist(self):
        response = self.client.get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_accordion_jqueryui_page_url_is_exist(self):
        response = self.client.get('/accordion/jqueryui')
        self.assertEqual(response.status_code, 301)

    def test_accordion_views_render_the_correct_template(self):
        response = self.client.get('/accordion/')
        self.assertTemplateUsed(response, 'accordionpage-templates/accordion.html')

    # ini error gara-gara gua make alert di jsnya, udh diilangin tp pas dibuka msh ada alertnya
    # kayanya masalah kukis kukisan sy gapahm
    def test_accordion_jqueryui_views_render_the_correct_template(self):
        response = self.client.get('/accordion/jqueryui/')
        self.assertTemplateUsed(response, 'accordionpage-templates/accordion-jqueryui.html')

    def test_accordion_using_the_right_views_function(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, accordion)

    def test_accordion_jqueryui_using_the_right_views_function(self):
        found = resolve('/accordion/jqueryui/')
        self.assertEqual(found.func, accordion_jqueryui)

class StatusFeedFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_accordion(self):
        self.driver.get(self.live_server_url + '/accordion/')

        time.sleep(5)

        # check the active accordion (which is accordion-1 by default)
        # therefore no need to click
        accordion_first = self.driver.find_element_by_id('accordion-1')
        self.assertIn('nonton drakor lah gilak ngapain lg 5 bulan liburrrr', self.driver.page_source)

        time.sleep(1)

        # self.assertNotIn('banyak dah sibuk bgt pokoknyaaa gamuatttt', self.driver.page_source)
        accordion_second = self.driver.find_element_by_id('accordion-2')
        accordion_second.click()
        self.assertIn('banyak dah sibuk bgt pokoknyaaa gamuatttt', self.driver.page_source)

        time.sleep(1)

        # self.assertNotIn('gaada :(', self.driver.page_source)
        accordion_third = self.driver.find_element_by_id('accordion-3')
        accordion_third.click()
        self.assertIn('gaada :(', self.driver.page_source)

        time.sleep(1)

        # self.assertNotIn('weeei cek letterboxd guaaaa ayo kita mutualan', self.driver.page_source)
        accordion_fourth = self.driver.find_element_by_id('accordion-4')
        accordion_fourth.click()
        self.assertIn('ayo kita mutualan', self.driver.page_source)

