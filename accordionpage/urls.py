from . import views
from django.urls import path

app_name='accordionpage'

urlpatterns = [
    path('', views.accordion, name='accordion'),
    path('jqueryui/', views.accordion_jqueryui, name='accordion_jqueryui')
]