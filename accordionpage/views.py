from django.shortcuts import render

# Create your views here.
def accordion(request):
    return render(request, 'accordionpage-templates/accordion.html')

def accordion_jqueryui(request):
    return render(request, 'accordionpage-templates/accordion-jqueryui.html')