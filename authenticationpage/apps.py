from django.apps import AppConfig


class AuthenticationpageConfig(AppConfig):
    name = 'authenticationpage'
