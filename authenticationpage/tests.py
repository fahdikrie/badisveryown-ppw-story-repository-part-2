from django.test import TestCase, override_settings, Client, LiveServerTestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from .apps import AuthenticationpageConfig
from .views import login_views, sign_up_views, logout_views
from django.contrib.auth.models import User


# Create your tests here.
class AuthenticationUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username='badibos', password='ehrahasiadong')

    def test_apps(self):
        self.assertEqual(AuthenticationpageConfig.name, 'authenticationpage')

    '''
    test if url is exist
    '''
    def test_login_url_is_exist(self):
        response = self.client.get('/auth/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_is_exist(self):
        response = self.client.get('/auth/signup/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_is_exist(self):
        response = self.client.get('/auth/logout/')
        self.assertEqual(response.status_code, 302)

    '''
    test if the urls are using the right function
    '''
    def test_login_using_the_right_function(self):
        found = resolve('/auth/login/')
        self.assertEqual(found.func, login_views)

    def test_signup_using_the_right_function(self):
        found = resolve('/auth/signup/')
        self.assertEqual(found.func, sign_up_views)

    def test_logout_using_the_right_function(self):
        found = resolve('/auth/logout/')
        self.assertEqual(found.func, logout_views)

    '''
    test using post method on both login & signup
    '''
    def test_logging_in(self):
        response = self.client.post("/auth/login/", follow=True, data={
                        "username": "badibos",
                        "password": "ehrahasiadong"
                    })

        # self.assertRedirects(response, '/auth/welcome/badibos', status_code=301,
        #                      target_status_code=301, fetch_redirect_response=True)
        self.assertContains(response, 'badibos')

    def test_signing_up(self):
        response = self.client.post("/auth/signup/", follow=True, data={
                        "username": "ameng",
                        "password1": "sobari",
                        "password2": "sobari"
                    })

        count = User.objects.count()
        # self.assertRedirects(response, '/auth/login/', status_code=301,
        #                      target_status_code=301, fetch_redirect_response=True)
        self.assertEqual(count, 1)


class AuthenticationFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_auth_functionality_using_selenium(self):
        self.driver.get(self.live_server_url + '/auth/signup/')

        '''
        Signing In
        '''
        time.sleep(5)
        self.driver.find_element_by_id("id_username").send_keys("badibadibadi")
        time.sleep(5)
        self.driver.find_element_by_id("id_password1").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_id("id_password2").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_id("signup-btn").click()
        time.sleep(5)
        self.assertIn('login page', self.driver.page_source)
        time.sleep(5)

        '''
        Logging In
        '''
        self.driver.find_element_by_id("id_username").send_keys("badibadibadi")
        time.sleep(5)
        self.driver.find_element_by_id("id_password").send_keys("ehrahasiadong")
        time.sleep(5)
        self.driver.find_element_by_name("login").click()
        time.sleep(5)
        self.assertIn('badibadibadi', self.driver.page_source)

