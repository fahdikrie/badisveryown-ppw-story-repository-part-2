import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'authenticationpage'

urlpatterns = [
    path('login/', views.login_views, name='login'),
    path('signup/', views.sign_up_views, name='signup'),
    path('logout/', views.logout_views, name='logout'),
    path('welcome/<str:username>', views.welcome_views, name='welcome')
]

'''
in case kalo mau pake builtin django auth urls
'''
# urlpatterns += [
#     path('accounts/', include('django.contrib.auth.urls')),
# ]

'''
jadinya /auth/accounts/ + (login, logout, password_change, etc.)
'''