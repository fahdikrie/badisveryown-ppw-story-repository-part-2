from django.shortcuts import render, redirect
from django.contrib import messages

from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout


# Create your views here.
def login_views(request):
    '''
    fungsi buat login
    '''
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            user = form.get_user()
            login(request, user)
            return redirect('authenticationpage:welcome', username)
    else:
        form = AuthenticationForm()

    return render(request, 'authenticationpage-templates/login.html', {'form':form})


def welcome_views(request, username):
    '''
    fungsi buat kalo login trs disambut welcome
    '''
    if not request.user.is_authenticated:
        return redirect('/')

    # header_username = username
    header_username = request.user.username # more failproof

    return render(request, 'authenticationpage-templates/welcome.html', {'username':header_username})


def sign_up_views(request):
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        '''
        fungsi buat sign up
        '''
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('authenticationpage:login')
    else:
        form = UserCreationForm()

    return render(request, 'authenticationpage-templates/signup.html', {'form':form})


def logout_views(request):
    '''
    fungsi buat logout
    '''
    logout(request)
    return redirect('/')
