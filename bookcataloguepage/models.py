from django.db import models

# Create your models here.
class LikedBook(models.Model):
    book_id = models.CharField(max_length=20)
    title = models.CharField(max_length=255)
    numberoflikes = models.PositiveIntegerField(default=1, blank=True, null=True)

    # def __str__(self):
    #     return "Title: " + self.title