from rest_framework import serializers
from .models import LikedBook

class LikedBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikedBook
        fields = '__all__'
