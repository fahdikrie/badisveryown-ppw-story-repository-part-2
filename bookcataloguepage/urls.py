from . import views
from django.urls import path, re_path

app_name='bookcataloguepage'

urlpatterns = [
    path('', views.bookcatalogue, name='bookcatalogue'),
    re_path(r'^apilikedbook/$', views.LikedBookViews.as_view()),
    path('apilikedbook/<int:pk>/', views.LikedBookSingleViews.as_view())
]