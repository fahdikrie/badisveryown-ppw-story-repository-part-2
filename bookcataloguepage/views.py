from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404

from .models import LikedBook
from .serializers import LikedBookSerializer
from django.shortcuts import render

# Create your views here.
def bookcatalogue(request):
    return render(request, 'bookcataloguepage-templates/catalogue-landing.html')

class LikedBookViews(APIView):
    def get(self, request):
        liked_books = LikedBook.objects.all()
        serialized_liked_books = LikedBookSerializer(liked_books, many=True)

        return Response(serialized_liked_books.data, status=status.HTTP_200_OK)

    # kayanya ga bakal ngepost di post cbv ini
    # tp ngepost di singleview
    # tp i'm keeping the numberoflikes block of code ykno just in case
    def post(self, request):
        data = request.data
        deserialized_data = LikedBookSerializer(data=data)
        if deserialized_data.is_valid():
            liked_book = deserialized_data.save()
            serialized_data = LikedBookSerializer(liked_book)
            return Response(serialized_data.data, status=status.HTTP_201_CREATED)
        return Response({'error': 'Data tidak valid'}, status=status.HTTP_400_BAD_REQUEST)

class LikedBookSingleViews(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return LikedBook.objects.get(pk=pk)
        except LikedBook.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        single_liked_book = self.get_object(pk)
        serialized_single_book = LikedBookSerializer(single_liked_book)
        return Response(serialized_single_book.data)

    def post(self, request, pk):
        single_liked_book = self.get_object(pk)
        serialized_single_book = LikedBookSerializer(single_liked_book)
        if serialized_single_book.is_valid():
            liked_book = serialized_single_book.save()
            return Response(serialized_single_book.data, status=status.HTTP_201_CREATED)
        return Response({'error': 'Data tidak valid'}, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        single_liked_book = self.get_object(pk)
        serialized_single_book = LikedBookSerializer(single_liked_book, data=request.data)
        if serialized_single_book.is_valid():
            serialized_single_book.save()
            return Response(serialized_single_book.data)
        return Response(serialized_single_book.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        single_liked_book = self.get_object(pk)
        serialized_single_book = LikedBookSerializer(single_liked_book, data=request.data, partial=True) # set partial=True to update a data partially
        if serialized_single_book.is_valid():
            serialized_single_book.save()
            return Response(serialized_single_book.data)
        return Response(serialized_single_book.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        single_liked_book = self.get_object(pk)
        single_liked_book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# ============ contoh generic nih wat bljr ============= #

# class LikedBookViews(generics.ListCreateAPIView):
#     queryset = LikedBook.objects.all()
#     serializer_class = LikedBookSerializer


# class LikedBookSingleViews(generics.RetrieveUpdateDestroyAPIView):
#     queryset = LikedBook.objects.all()
#     serializer_class = LikedBookSerializer

# ============ gajadi dipake ============= #

# class LikedBookSingleViews(APIView):
#     """
#     Retrieve, update or delete a snippet instance.
#     """
#     def get(self, request, id):
#         single_liked_book = LikedBook.objects.get(id=1)
#         serialized_single_liked_book = LikedBookSerializer(single_liked_book, many=True)

#         return Response(serialized_single_liked_book.data, status=status.HTTP_200_OK)

#     def post(self, request, id):
#         single_liked_book = LikedBook.objects.get(id=1)

#         deserialized_data = LikedBookSerializer(data=data)
#         if deserialized_data.is_valid():
#             liked_book = deserialized_data.save()
#             serialized_data = LikedBookSerializer(liked_book)
#             return Response(serialized_data.data, status=status.HTTP_201_CREATED)
#         return Response({'error': 'Data tidak valid'}, status=status.HTTP_400_BAD_REQUEST)