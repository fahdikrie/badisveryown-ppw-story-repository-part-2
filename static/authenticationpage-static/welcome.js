$(document).ready(function() {

  textSequence(0);
  timer();
  timerSequence();
});

const timer = () => {
  setTimeout(function() {
    window.location='/'
  }, 8000);
};

const textSequence = (i) => {
  let welcomeArray = ['bienvenue', '어서 오십시오', 'wilkommen', 'ようこそ']

  if (welcomeArray.length > i) {
    setTimeout(function() {
        // document.getElementById("welcome-tag").innerHTML = welcomeArray[i];
        $('#welcome-tag').text(welcomeArray[i])
        // console.log($('#welcome-tag').html())
        textSequence(++i);
    }, 1500); // 1 second (in milliseconds)

  } else if (welcomeArray.length == i) { // Loop
      textSequence(0);
  }
};

const timerSequence = () => {
  let currentTimer = $('#countdown-timer').text()
  console.log(currentTimer)

  if (Number(currentTimer) > 1) {
    setTimeout(function() {
      $('#countdown-timer').text(Number(currentTimer) - 1);
      timerSequence();
    }, 1000)
  } else {
    setTimeout(function() {
      $('#countdown-timer').text(Number(currentTimer) - 1);
    }, 1000)
  }
}