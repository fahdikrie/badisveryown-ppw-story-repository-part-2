const bookCatalogue = () => {
  let query = $('#search-input').val()
  console.log(query);
  $('#table-body').empty()

  $.ajax({
    url: 'https://www.googleapis.com/books/v1/volumes?q=' + query,
    dataType: 'json',
    type: 'GET',

    success: function(data) {
      let tableContainer = $('#table-body')

      for (i = 0; i < data.items.length; i++) {
        let tempDataObj = data.items[i].volumeInfo

        // let imgCol =''
        // if (tempDataObj.imageLinks.smallThumbnail != '') {
        //   imgCol = '<th scope="row"><img src="' + tempDataObj.imageLinks.smallThumbnail + '"></img></th>'
        // }
        // else {
        //   imgCol = '<th scope="row"><img src="' + 'https://bitsofco.de/content/images/2018/12/Screenshot-2018-12-16-at-21.06.29.png' + '"></img></th>'
        // }

        let imgCol = ''
        try {
          imgCol = '<th scope="row"><img height="150px" width="100px" src="' + tempDataObj.imageLinks.smallThumbnail + '"></img></th>'
        }
        catch (TypeError) {
          imgCol = '<th scope="row"><img height="150px" width="100px" src="' + 'http://books.google.com/books/content?id=notfound&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api' + '"></img></th>'
        }

        let titleCol = '<td class="title-col"><p>' + tempDataObj.title + '</p></td>'
        let authorCol = '<td><p>' + tempDataObj.authors + '</p></td>'
        let idCol = '<td class="id-col"><p><a style="text-decoration: underline;" target="_blank" href="https://books.google.com/ebooks?id=' + data.items[i].id +  '">' + data.items[i].id + + '</a></p></td>'
        let likeButton = '<td class="like-col">' + '<a><i class="like-button fa fa-thumbs-up fa-2x" aria-hidden="true" style="display: inline;"></i></a>' + '</td>'

        tableContainer.append(
            '<tr>' + imgCol + titleCol + authorCol + idCol + likeButton + '</tr>'
        )
      }
    },

    error: function () {
    // mamamamardiaaallll
    }
  });
}


$('#search-input').keypress(function (e) {
  if (e.keyCode == 13) {
    bookCatalogue()
  }
})

$('#search-button').click(bookCatalogue)

