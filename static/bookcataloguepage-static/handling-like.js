$(document).ready(function() {
  $(document).on("click", ".like-button", function () {
    let td_id = $(this).parent().parent().parent()
    let clicked_book_id = td_id.find('.id-col').children().children().text()
    let clicked_title = td_id.find('.title-col').children().text()
    console.log(clicked_book_id)
    console.log(clicked_title)

    animateLikePlusOne($(this));

    $.ajax({
      url: "/bookcatalogue/apilikedbook/",
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        likedBooks = data
        // console.log(likedBooks[2].numberoflikes)

        let listOfBookID = []
        likedBooks.forEach(element => {
          listOfBookID.push(element.book_id)
        });

        // console.log(listOfBookID)
        // console.log(isInstanceOfArray(listOfBookID, "znBngTKxSAECNaN"))

        // kalo udah ada dia increment likenya aja pake patch/put
        if (isInstanceOfArray(listOfBookID, clicked_book_id)) {
          let id = 0
          let numberoflikes = 0

          likedBooks.forEach(element => {
            if (element.book_id == clicked_book_id) {
              id = element.id
              numberoflikes = element.numberoflikes
            }
          });

          incrementLike(clicked_book_id, clicked_title, id, numberoflikes)
        }

        // kalo blm ada dia ngepost ke api bikin baru gituuu
        else {
          postAPI(clicked_book_id, clicked_title)
        }
      }
    });
  });
});

const postAPI = (book_id, title) => {
  $.ajax({
    url: "/bookcatalogue/apilikedbook/",
    type: 'post',
    dataType: 'json',
    data: {
      'book_id': book_id,
      'title': title,
      'numberoflikes': 1,
      // csrfmiddlewaretoken: window.CSRF_TOKEN
      // csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
      'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val()
    },
    success: function(data){
      console.log( 'success, server says '+data);
    },
    error: function(data) {
      console.log("error")
    }
  })
}

const incrementLike = (book_id, title, id, like) => {
  let incrementedLike = Number(like) + 1;
  console.log(incrementedLike)

  $.ajax({
    url: "/bookcatalogue/apilikedbook/" + id +'/',
    // type: 'PUT',
    type: 'patch',
    dataType: 'json',
    // contentType: "application/json",
    data: {
      'book_id': book_id,
      'title': title,
      'numberoflikes': incrementedLike,
      'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val()
    }
  })
}

const isInstanceOfArray = (array_name, object) => {
  let bool = false
  for (i = 0; i < array_name.length; i++) {
    if (array_name[i] == object) {
      bool = true;
      break;
    }
  }
  return bool;
}

const animateLikePlusOne = (param) => {
  let clicked_likedButton = param
  let likeButtonCol = clicked_likedButton.parent()

  clicked_likedButton.addClass('active')
  likeButtonCol.append('<p class="ml-2" style="display: inline;">+1</p>')
  setTimeout(() => {
    clicked_likedButton.removeClass('active')
    likeButtonCol.find('p').remove()
  }, 500);
}

// $('.like-button').click(function() {
//   alert("haha")
//   let book_id = $(this).parent().prev().val()
//   console.log(book_id)
// })

// $('.like-col').on("click", '.like-button' , function() {
//   console.log("weh keklik");
// });



// $(document).ready ( function () {
//   $(document).on ("click", "#littleNews", function () {
//       alert("hi");
//   });
// });

// $( "#dataTable tbody tr" ).on( "click", function() {
//   console.log( $( this ).text() );
// });

// const fetchAllLikedBooks = () => {
//   let likedBooks = []
//   $.ajax({
//     url: "/bookcatalogue/apilikedbook/",
//     type: 'GET',
//     dataType: 'json',
//     success: function (data) {
//       likedBooks = data
//       console.log("success")
//       console.log(likedBooks[2].numberoflikes)

//       let listOfBookID = []
//       likedBooks.forEach(element => {
//         listOfBookID.push(element.book_id)
//       });

//       console.log(listOfBookID)
//       console.log(isInstanceOfArray(listOfBookID, "znBngTKxSAECNaN"))

//       if (isInstanceOfArray(listOfBookID, book_id)) {
//         incrementLike(book_id, id, numberoflikes)
//       } else {
//         postAPI(book_id, title)
//       }
//     }
//   });
//   return likedBooks;
// };

// let likedBooks = fetchAllLikedBooks()
// console.log(likedBooks)