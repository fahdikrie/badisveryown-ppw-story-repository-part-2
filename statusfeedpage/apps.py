from django.apps import AppConfig


class StatusfeedpageConfig(AppConfig):
    name = 'statusfeedpage'
