from django import forms

class StatusForm(forms.Form):
    posted_by = forms.CharField(max_length=30, label='name',widget=forms.TextInput(
        attrs={
            'id' : 'posted_by',
            'class' : 'status_feed_form',
            'placeholder' : 'write your name here dearie',
            'style' : 'width: 50vw;'
        },
    ))

    status = forms.CharField(max_length=355, label='status', widget=forms.Textarea(
        attrs={
            'id' : 'status',
            'class' : 'status_feed_form',
            'placeholder' : 'how dya feelin today lad?',
            'style' : 'width: 50vw;'
        },
    ))