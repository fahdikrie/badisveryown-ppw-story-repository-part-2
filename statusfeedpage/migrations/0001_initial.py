# Generated by Django 3.0.3 on 2020-04-12 02:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StatusFeed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.TextField(max_length=355)),
                ('posted_by', models.CharField(max_length=30)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
