from django.db import models

# Create your models here.

# Buat class yg bisa bikin status
# > Status 
# > Date and Time (?)

class StatusFeed(models.Model):
    status = models.TextField(max_length=355)
    posted_by = models.CharField(max_length=30)
    datetime = models.DateTimeField(auto_now_add=True)

    color = models.CharField(max_length=9, default="#000000", null=True) # aaaaaa

    def __str__(self):
        return "posted by: {}, status: {}".format(self.posted_by, self.status)
