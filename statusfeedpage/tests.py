from django.test import TestCase, override_settings, Client, LiveServerTestCase
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .apps import StatusfeedpageConfig
from .views import status_feed
from .models import StatusFeed


# Create your tests here.
class StatusFeedUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.status_update = StatusFeed.objects.create(status='samlekom mamang', posted_by='badi', datetime=timezone.now(), color='cadetblue')

    def test_apps(self):
        self.assertEqual(StatusfeedpageConfig.name, 'statusfeedpage')

    def test_status_feed_url_is_exist(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_status_feed_views_render_the_correct_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'statusfeedpage-templates/feed.html')

    def test_status_feed_template_contains(self):
        response = self.client.get('/')
        self.assertContains(response, "(What's The Story) Morning Glory?", html=True) # ...... To Be Continued

    def test_status_feed_models(self):
        statusCount = StatusFeed.objects.count()
        self.assertEqual(statusCount, 1)

    def test_status_feed_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/', {
                'status': 'bbbbbbboker',
                'posted_by': 'bbbadi',
                'color': 'cadetblue'
            }
        )
        self.assertEqual(response_post.status_code, 302)

class StatusFeedFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_status_functional_post_status_confirmed(self):
        self.driver.get(self.live_server_url)

        time.sleep(3)

        self.assertIn("(What's The Story) Morning Glory?", self.driver.page_source)

        posted_by = self.driver.find_element_by_id('posted_by')
        posted_by.send_keys('badiiiiii')

        status = self.driver.find_element_by_id('status')
        status.send_keys('assalamualaikum! mamang!')

        button = self.driver.find_element_by_id('submit')
        button.click()

        time.sleep(5)

        self.assertIn('are u suuuuuuuure', self.driver.page_source)

        confirm_button = self.driver.find_element_by_id('confirm')
        confirm_button.click()

        self.assertIn("badiiiiii", self.driver.page_source)
        self.assertIn("assalamualaikum! mamang!", self.driver.page_source)

    def test_status_functional_post_status_canceled(self):
        self.driver.get(self.live_server_url)

        time.sleep(3)

        self.assertIn("(What's The Story) Morning Glory?", self.driver.page_source)

        posted_by = self.driver.find_element_by_id('posted_by')
        posted_by.send_keys('bbbbadi')

        status = self.driver.find_element_by_id('status')
        status.send_keys('waalaikumsalam! mamang!')

        button = self.driver.find_element_by_id('submit')
        button.click()

        time.sleep(5)

        self.assertIn('are u suuuuuuuure', self.driver.page_source)

        decline_button = self.driver.find_element_by_id('decline')
        decline_button.click()

        self.assertNotIn("bbbbadi", self.driver.page_source)
        self.assertNotIn("waalaikumsalam! mamang!", self.driver.page_source)

    def test_status_functional_change_bubble_color(self):
        new_status = StatusFeed.objects.create(status="tes", posted_by="badiii")
        new_status.save()

        self.driver.get(self.live_server_url)

        time.sleep(3)

        # pre_changed = self.driver.find_element_by_class_name('bubble').getCssValue('background-color')
        pre_changed = self.driver.find_element_by_class_name('bubble').value_of_css_property('background-color')

        toggle_color = self.driver.find_element_by_class_name('toggle_color')
        toggle_color.click()

        # post_changed = self.driver.find_element_by_class_name('bubble').getCssValue('background-color')
        post_changed = self.driver.find_element_by_class_name('bubble').value_of_css_property('background-color')


        self.assertNotEqual(pre_changed, post_changed)