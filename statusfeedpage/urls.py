from . import views
from django.urls import path

app_name='statusfeedpage'

urlpatterns = [
    path('', views.status_feed, name='status_feed'),
    path('confirmation/<str:status>/<str:posted_by>', views.status_feed_confirmation, name='status_feed_confirmation'),
    path('change_color/<int:id>', views.status_feed_change_color, name='status_feed_change_color')
]