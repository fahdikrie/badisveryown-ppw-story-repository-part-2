from django.shortcuts import render, HttpResponseRedirect, redirect
from .models import StatusFeed
from .forms import StatusForm

# Create your views here.

def status_feed(request):
    posted_statuses = StatusFeed.objects.all().values().order_by('datetime').reverse()
    form = StatusForm(request.POST or None)

    context = {
        'posted_statuses' : posted_statuses,
        'form' : form
    }

    if request.method == 'POST':
        status = request.POST.get('status')
        posted_by = request.POST.get('posted_by')

        context = {
            'status' : status,
            'posted_by' : posted_by
        }

        if form.is_valid():
            # return render(request, 'statusfeedpage-templates/confirmation.html', context, posted_by)
            return redirect('statusfeedpage:status_feed_confirmation', status, posted_by)

    return render(request, 'statusfeedpage-templates/feed.html', context)

def status_feed_confirmation(request, status, posted_by):
    context = {
        'status' : status,
        'posted_by' : posted_by
    }

    if request.method == 'POST':
        StatusFeed.objects.create(
                        posted_by=status,
                        status=posted_by
                    )

        return redirect('statusfeedpage:status_feed')

    return render(request, 'statusfeedpage-templates/confirmation.html', context)


def status_feed_change_color(request, id):
    # status_bubble = StatusFeed.objects.all().filter(id=id)
    status_bubble = StatusFeed.objects.get(id=id)
    pre_color = status_bubble.color

    colors = ['#000000','#8c348b','#3872c4', '#222222', '#c43838']

    index_pre_color = colors.index(pre_color)

    if index_pre_color != 4:
        index_post_color = index_pre_color + 1
    else:
        index_post_color = 0

    # try:
    #     index_post_color = index_pre_color + 1
    
    # except IndexError:
    #     index_post_color = 0

    # try:
    #     colors.index(g)
    #     iterator = iter(colors)
    #     pre_color = next(iterator)
    # except StopIteration:
    #     p

    post_color = colors[index_post_color]
    status_bubble.color = post_color
    status_bubble.save()

    return redirect('/')
